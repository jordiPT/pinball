﻿using UnityEngine;
using System.Collections;

public class Pitcher : MonoBehaviour {

    public float counterMax = 4;
    public float speed = 1;
    public float strenght = 1500;
    public GameObject ball;
    private Rigidbody rb;
    public bool playing = false;

    public float counter = 0;

	// Use this for initialization
	void Start () {
        rb = ball.GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        

            if (Input.GetKey(KeyCode.Space))
            {
                if (counter < 1)
                {
                    transform.Translate(0, 0, speed * Time.deltaTime);
                    counter += speed * Time.deltaTime;
                }
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                transform.Translate(0, 0, -30 * Time.deltaTime);
                counter = 0;
                if (!playing)
                {
                    rb.AddForce(-transform.forward * 8000);
                    playing = true;
                }
            }

        
	}
}
