﻿using UnityEngine;
using System.Collections;

public class LeftFlipper : MonoBehaviour {

    HingeJoint hinge;
    Rigidbody rb;
    float speed = 1;
    Vector3 initialPosition;
    Quaternion initialRotation;
    float maxAngle = 0;

	// Use this for initialization
	void Start () {
        hinge = GetComponent<HingeJoint>();
        rb = GetComponent<Rigidbody>();
        initialPosition = transform.position;
        
	}
	
	// Update is called once per frame
	void Update () {

        hinge.useLimits = true;

        if (Input.GetKey(KeyCode.LeftArrow))
        {   
                rb.AddForce(-transform.right * 6000);
        }
        else
        {
            
            if(transform.position == initialPosition)
            {
                return;
            }
            else
            {
                rb.AddForce(transform.right * 6000);
            }
        }

        
        
	}
}
