﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    Rigidbody rb;
    float power = 500;
    Vector3 position;

    public GameObject sc;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bumper") {

            position  = transform.position;
            Vector3 direction = collision.contacts[0].point - position; //cojo direccion del vector mediante los puntos de contacto
            direction = -direction.normalized; //invierto la direccion
            rb.AddForce(direction * power); //le aplico la fuerza en esa direccion
           // s.score = s.score + 100;
        }

    }
}
