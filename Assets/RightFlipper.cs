﻿using UnityEngine;
using System.Collections;

public class RightFlipper : MonoBehaviour {

    HingeJoint hinge;
    Rigidbody rb;
    float speed = 2;
    Vector3 initialPosition;

	// Use this for initialization
	void Start () {
        hinge = GetComponent<HingeJoint>();
        rb = GetComponent<Rigidbody>();
        initialPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        hinge.useLimits = true;

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddForce(transform.right * 6000);
        } 
        else
        {

            if (transform.position == initialPosition)
            {
                return;
            }
            else
            {
                rb.AddForce(-transform.right * 6000);
            }
        }

	}
}
