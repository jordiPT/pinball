﻿using UnityEngine;
using System.Collections;

public class Target3 : MonoBehaviour {
    private float cont = 60f;
    bool col = false;
    public GameObject target;
    Quaternion initialRotation;


	// Use this for initialization
	void Start () {
        initialRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
	if (col)
        {
            
            if (transform.rotation.eulerAngles.y == 180 && transform.rotation.eulerAngles.z == 180)
            {
                if (cont > 0)
                {
                    Quaternion newOrientation = transform.rotation; 
                    transform.rotation = newOrientation;
                
                    cont = cont - 1f;
                }
                else
                {
                    transform.rotation = initialRotation;
                    col = false;
                    cont = 60f;
                }


            }
            else 
            {
                this.transform.Rotate(-2f, 0, 0);
                
                
            }
            
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            //s.score = s.score + 50;
            col = true;
        }

    }
}
